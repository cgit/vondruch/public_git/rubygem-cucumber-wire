# Generated from cucumber-wire-0.0.1.gem by gem2rpm -*- rpm-spec -*-
%global gem_name cucumber-wire

%{?_with_bootstrap: %global bootstrap 1}

Name: rubygem-%{gem_name}
Version: 0.0.1
Release: 1%{?dist}
Summary: Wire protocol for Cucumber
Group: Development/Languages
License: MIT
URL: http://cucumber.io
Source0: https://rubygems.org/gems/%{gem_name}-%{version}.gem
BuildRequires: ruby(release)
BuildRequires: rubygems-devel
BuildRequires: ruby
%if ! 0%{?bootstrap}
# Dependencies for %%check
BuildRequires: rubygem(aruba)
BuildRequires: rubygem(cucumber)
BuildRequires: rubygem(rspec)
%endif
BuildArch: noarch

%description
Wire protocol for Cucumber.


%package doc
Summary: Documentation for %{name}
Group: Documentation
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}.

%prep
gem unpack %{SOURCE0}

%setup -q -D -T -n  %{gem_name}-%{version}

gem spec %{SOURCE0} -l --ruby > %{gem_name}.gemspec

%build
gem build %{gem_name}.gemspec

%gem_install

%install
mkdir -p %{buildroot}%{gem_dir}
cp -a .%{gem_dir}/* \
        %{buildroot}%{gem_dir}/


%if ! 0%{?bootstrap}
%check
pushd .%{gem_instdir}
LANG=C.UTF-8 rspec spec

# Not exactly sure why this is failing :/
sed -i "/(Timeout::Error)/ a\            /usr/share/ruby/timeout.rb:106:in \`timeout'" features/timeouts.feature
cucumber
popd
%endif

%files
%dir %{gem_instdir}
%exclude %{gem_instdir}/.*
%{gem_libdir}
%exclude %{gem_cache}
%{gem_spec}

%files doc
%doc %{gem_docdir}
%{gem_instdir}/Gemfile
%doc %{gem_instdir}/README.md
%{gem_instdir}/Rakefile
%{gem_instdir}/cucumber-wire.gemspec
%{gem_instdir}/features
%{gem_instdir}/spec

%changelog
* Tue Apr 05 2016 Vít Ondruch <vondruch@redhat.com> - 0.0.1-1
- Initial package
